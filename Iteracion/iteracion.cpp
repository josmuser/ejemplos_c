#include<iostream>
using namespace std;

int main(){
	//ejercicio de repeticion o iteracion
	//Cuando nos referimos iteracion es cuando algo incrementa y todo empieza por un valor nulo ejemplo., 0,1,2,etc.
	//Ejemplo de la implementacion de un for
	int dato=0;
	int miopcion;
	cout<<"Ingrese el numero que dese iterar\n";
	cout<<"Digite un numero\n";
	cout<<">";cin>>dato;
    cout<<"Cuantas veces desea iterar\n";
	cout<<">";cin>>miopcion;
	for(int i=0;i<miopcion;i++){
		cout<<"Iterando numero...espere\n";
		dato++;
	}
	cout<<"Su numero fue iterado "<<miopcion<<" veces\nvalor final:"<<dato;
	return 0;
}
